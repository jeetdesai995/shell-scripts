# [File Search Script](https://gitlab.com/jeetdesai995/shell-scripts/-/blob/master/find-file.sh)

## Description

This shell script searches for files in a specified directory and its subdirectories based on a given file name. It utilizes the `find` command to perform the search and prints the paths of matching files.

## Usage

```bash
./file_file.sh <directory> <file_name>
```

- `<directory>`: The directory where the search begins.
- `<file_name>`: The file by name to search for.


# [File Compare Script](https://gitlab.com/jeetdesai995/shell-scripts/-/blob/master/compare-file.sh)

## Description

This shell script compare two file and It utilizes the `sdiff` command to perform the compare.

## Usage

```bash
./compare-file.sh <file_path> <file_path>
```

- `<file_path>`: The file path with name to compare for. 



